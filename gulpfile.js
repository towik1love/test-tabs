let gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify-es').default,
    rename = require('gulp-rename'),
    uglifycss = require('gulp-uglifycss'),
    concat = require('gulp-concat');


gulp.task('scripts', function () {
    return gulp.src([
        './src/js/jquery.2.2.min.js',
        './src/js/script.js'])
        .pipe(uglify())
        .pipe(concat('all.js'))
        .pipe(rename('scripts.min.js'))
        .pipe(gulp.dest('./src/js'));
});

gulp.task('sass', function () {
    return gulp.src('src/scss/style.scss')
        .pipe(rename('style.css'))
        .pipe(sass().on('error', sass.logError))
        .pipe(uglifycss({
            "uglyComments": true
        }))
        .pipe(gulp.dest('./src/css/'));
});

gulp.task('watch', function () {
    gulp.watch(['src/scss/*.scss', 'src/scss/*/*.scss'], gulp.series('sass'));
    gulp.watch('./src/js/script.js', gulp.series('scripts'));
});
