jQuery(function ($) {
    window.fstack = {};
    $.extend(window.fstack, {
        init: function () {
            this.tabFunc();
        },

        tabFunc: function () {
            let _tab = $('.tabs-list .tab-content')
            let _a = 'active'
            let setState = (a, tab) => {
                let _plus = 'lnr-plus-circle'
                let _minus = 'lnr-circle-minus'
                if (tab.hasClass(window.location.hash.split('#')[1])) {
                    tab.removeClass(a);
                    $('.tabs-list').find(`.${window.location.hash.split('#')[1]}`).addClass(a);
                }
                tab.each(function () {
                    $(this).hasClass(a) ? $(this).find('h2 span').addClass(_minus).removeClass(_plus) : $(this).find('h2 span').addClass(_plus).removeClass(_minus);
                });
            }
            if (window.location.hash === '') {
                window.location.hash = '#general'
            } else {
                setState(_a, _tab)
            }

            window.onhashchange = () => {
                setState(_a, _tab)
            }

        }
    })
    fstack.init();
});